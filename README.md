# Germix React Sidebar

## About

Germix react sidebar component

## Installation

```bash
npm install @germix/germix-react-sidebar
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
