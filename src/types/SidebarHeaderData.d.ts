
/**
 * @author Germán Martínez
 */
interface SidebarHeaderData
{
    logo,
    label,
}
export default SidebarHeaderData;
