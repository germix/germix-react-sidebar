
/**
 * @author Germán Martínez
 */
interface SidebarItemData
{
    icon?,
    label,
    route?,
    badge?: { label, type? },
    children?: SidebarItemData[],
}
export default SidebarItemData;
