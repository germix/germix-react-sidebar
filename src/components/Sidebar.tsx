import React from 'react';
import SidebarItem from './SidebarItem';
import SidebarSubmenu from './SidebarSubmenu';

import SidebarHeaderData from '../types/SidebarHeaderData';
import SidebarItemData from '../types/SidebarItemData';

interface Props
{
    path,
    submenuStateIcon: string,
    items: SidebarItemData[],
    header?: SidebarHeaderData,

    isItemActive?(item: SidebarItemData),

    onClickItem(item: SidebarItemData, e),
    onTranslateItem?(item: SidebarItemData),
}
interface State
{
    hover: boolean,
}

/**
 * @author Germán Martínez
 */
class Sidebar extends React.Component<Props,State>
{
    state: State = {
        hover: false,
    }
    ref;
    map;

    constructor(props)
    {
        super(props);

        let data;
        if(null != (data = this.loadStoredData()))
        {
            this.state.hover = data.hover;
        }
    }
    render()
    {
        let className = 'sidebar';

        if(this.state.hover)
        {
            className += ' hover';
        }

        return (
<div
    ref={(e) => this.ref = e}
    className={className}
    onMouseEnter={() =>
    {
        this.setState({
            hover: true,
        }, () =>
        {
            this.storeData();
        });
    }}
    onMouseLeave={() =>
    {
        this.setState({
            hover: false,
        }, () =>
        {
            this.storeData();
        });
    }}
    >
    { this.props.header &&
        <div className="sidebar-header">
            <img src={this.props.header.logo}/>
            <span>{this.props.header.label}</span>
        </div>
    }
    <div className="sidebar-content">
        {
            this.props.items.map((item, index) =>
            {
                if(!('children' in item))
                    return this.renderItem(item, index, 0);
                else
                    return this.renderSubmenu(item, index, 0);
            })
        }
    </div>
</div>
        );
    }
    componentDidMount()
    {
        let data;
        if(null != (data = this.loadStoredData()))
        {
            this.ref.scrollTo(data.scrollX, data.scrollY);
        }
    }
    componentWillUnmount()
    {
        this.storeData();
    }
    renderItem = (item, index, level) =>
    {
        const active = this.props.isItemActive
                        ? this.props.isItemActive(item)
                        : item.route === this.props.path
                        ;
        
        return (
            <SidebarItem
                key={index}
                item={item}
                level={level}
                active={active}
                onClickItem={this.props.onClickItem}
                onTranslateItem={this.onTranslateItem}
            ></SidebarItem>
        );
    }
    renderSubmenu = (item, index, level) =>
    {
        return (
            <SidebarSubmenu
                key={index}
                item={item}
                level={level}
                submenuStateIcon={this.props.submenuStateIcon}
                onTranslateItem={this.onTranslateItem}
            >
            {
                item.children.map((subitem, index) =>
                {
                    if(!('children' in subitem))
                        return this.renderItem(subitem, index, level+1);
                    else
                        return this.renderSubmenu(subitem, index, level+1);
                })
            }
            </SidebarSubmenu>
        );
    }

    private onTranslateItem = (item) =>
    {
        if(this.props.onTranslateItem)
            return this.props.onTranslateItem(item);
        return item.label;
    }

    private storeData = () =>
    {
        localStorage.setItem('sidebar', JSON.stringify(
            {
                hover: this.state.hover,
                scrollX: this.ref.scrollLeft,
                scrollY: this.ref.scrollTop,
            }));
    }

    private loadStoredData = () =>
    {
        var data:any = localStorage.getItem('sidebar');
        try
        {
            return JSON.parse(data);
        }
        catch(error)
        {
        }
        return null;
    }
};
export default Sidebar;
