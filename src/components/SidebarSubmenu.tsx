import React from 'react';
import SidebarItemData from '../types/SidebarItemData';

interface Props
{
    item?,
    level?,
    submenuStateIcon?,
    onTranslateItem(item: SidebarItemData),
}
interface State
{
    isOpen: boolean,
    contentHeight: number|'auto',
    isTransitioning: boolean,
}

/**
 * @author Germán Martínez
 */
class SidebarSubmenu extends React.Component<Props,State>
{
    state: State =
    {
        isOpen : false,
        contentHeight: 0,
        isTransitioning: false,
    }
    content;
    intervalId;

    constructor(props)
    {
        super(props);
        
        let storage = localStorage.getItem('sidebar-submenu[' + this.props.item.label + ']');
        if(storage != null)
        {
            let data = JSON.parse(storage);

            this.state.isOpen = data.open;
            if(this.state.isOpen)
            {
                this.state.contentHeight = 'auto';
            }
        }
    }
    render()
    {
        const { item } = this.props;

        const itemStyle : any =
        {
            '--level': (this.props.level),
        }
        const iconStyle =
        {
            transform: (this.state.isOpen) ? 'rotate(90deg)' : ''
        }
        const contentStyle =
        {
            height: this.state.contentHeight
        }
        return (
<div
    style={{ position : 'relative' }}
    className={"sidebar-submenu" + (this.state.isOpen ? ' open' : '')}
    >
    <div
        style={itemStyle}
        className="sidebar-item"
        onClick={ (e) =>
            {
                if(!this.state.isTransitioning)
                {
                    this.setState({
                        contentHeight: this.content.scrollHeight,
                    });
                    setTimeout(() =>
                    {
                        this.setState({
                            isOpen: !this.state.isOpen,
                            contentHeight: this.state.isOpen ? 0 : this.content.scrollHeight,
                            isTransitioning: true
                        });
                        this.intervalId = setInterval(() =>
                        {
                            if(!this.state.isOpen)
                            {
                                if(parseInt(this.content.offsetHeight) === 0)
                                {
                                    clearInterval(this.intervalId);
                                    
                                    this.setState({
                                        isTransitioning: false
                                    });
                                }
                            }
                            else
                            {
                                if(this.content.offsetHeight === this.content.scrollHeight)
                                {
                                    clearInterval(this.intervalId);
                                    
                                    this.setState({
                                        isTransitioning: false,
                                        contentHeight: 'auto'
                                    });
                                }
                            }
                        }, 10);
                    }, 10);
                }
            }}
        >
        <div className="sidebar-item-label">
            {item.icon && (<i className={item.icon} />) }
            <span>
            {this.props.onTranslateItem(item)}
            </span>
        </div>
        <i className={this.props.submenuStateIcon} style={iconStyle}></i>

        { (() =>
        {
            if(item.badge != undefined)
            {
                let className = 'sidebar-item-badge';
                if(item.badge.type)
                    className += ' ' + item.badge.type;

                return <div className={className}>{item.badge.label}</div>
            }
            return undefined;
        })() }
    </div>
    
    <div
        ref={(e) => this.content = e}
        className={"sidebar-content"}
        style={contentStyle}
    >
        { this.props.children }
    </div>
</div>
        );
    }
    componentDidMount()
    {
    }
    componentDidUpdate()
    {
        localStorage.setItem('sidebar-submenu[' + this.props.item.label + ']', 
            JSON.stringify({
                open : this.state.isOpen,
            })
        );
    }
};
export default SidebarSubmenu;
