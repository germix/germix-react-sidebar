import React from 'react';
import SidebarItemData from '../types/SidebarItemData';

interface Props
{
    item?,
    level?,
    active?,
    onClickItem(item: SidebarItemData, e),
    onTranslateItem(item: SidebarItemData),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class SidebarItem extends React.Component<Props,State>
{
    render()
    {
        const { item, level, active} = this.props;
        const style: any =
        {
            '--level': (level),
        };
        return (
<a
    href={item.route}
    style={style}
    className={"sidebar-item" + (active ? ' active' : '')}
    onClick={ (e) =>
    {
        e.preventDefault();
        e.stopPropagation();
        this.props.onClickItem(item, e);
    }}
    >
    <div className="sidebar-item-label">
        {item.icon && <i className={item.icon} /> }
        <span>
            {this.props.onTranslateItem(item)}
        </span>
    </div>
    { (() =>
    {
        if(item.badge !== undefined)
        {
            let className = 'sidebar-item-badge';
            if(item.badge.type)
                className += ' ' + item.badge.type;

            return <div className={className}>{item.badge.label}</div>
        }
        return undefined;
    })() }
</a>
        );
    }
};
export default SidebarItem;
